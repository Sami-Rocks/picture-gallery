import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SingupService } from './../../../../core/services/singup.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { ConfirmEqualValidatorDirective } from './../../../../shared/equal-value-validator';


@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {
  myForm: FormGroup;

  constructor( 
    private fb: FormBuilder,
    private signupService: SingupService,
    private router: Router,
    private toastr: ToastrService
) { }

  ngOnInit(){
    this.myForm = this.fb.group({
      email: ['', [
        Validators.required,
        Validators.email
      ]],
      first_name: ['', [
        Validators.required
      ]],
      last_name: ['', [
        Validators.required,
      ]],
      phone_number: ['', [
        Validators.required
      ]],
      password: ['', [
        Validators.required,
        Validators.pattern('^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9]+)$'),
        Validators.minLength(6)
      ]],
      password1: ['', [
        Validators.required,
      ]]
    })

    this.myForm.valueChanges.subscribe();
  }

  get email(){
    return this.myForm.get('email');
  }
  get phone_number(){
    return this.myForm.get('phone_number');
  }
  
  get password(){
    return this.myForm.get('password');
  }
  get password1(){
    return this.myForm.get('password1');
  }
  

  submitHandler(){
    const payload = this.myForm.value;

    this.signupService.signup(payload)
      .subscribe(
        next => {
          if (next === true ){
            this.toastr.success("Signup successfull!!!");
            this.router.navigate(['/', 'gallery']);
            console.log("signed up");
          }else{
            this.toastr.error("Signup failed!!!");
            console.log("failed");
          }
        }
      )
  }

}
