import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LoginService } from './../../../../core/services/login.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  myForm: FormGroup;

  constructor( 
    private fb: FormBuilder,
    private loginService: LoginService,
    private router: Router,
    private toastr: ToastrService
  ) { }

  ngOnInit(){
    this.myForm = this.fb.group({
      email: ['', [
        Validators.required,
        Validators.email
      ]],
      password: ['', [
        Validators.required,
        Validators.pattern('^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9]+)$'),
        Validators.minLength(6)
      ]]
    })

    this.myForm.valueChanges.subscribe();
  }

  get email(){
    return this.myForm.get('email');
  }
  
  get password(){
    return this.myForm.get('password');
  }
  

  submitHandler(){
    const payload = this.myForm.value;
    
    this.loginService.login(payload)
      .subscribe(
        next => {
          if (next === true){
            
            this.toastr.success("Login successfull!!!");
            console.log("logged in");
            this.router.navigate(['/', 'gallery']);
          }else{
            this.toastr.error("Login failed!!!");
            console.log("failed");
          }
        }
      )
  }

}
