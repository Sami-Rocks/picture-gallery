import { Component, OnInit } from '@angular/core';
import { URLS } from './../../../../core/services/constants.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { LogoutService } from './../../../../core/services/logout.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.scss']
})
export class WelcomeComponent implements OnInit {

  constructor(
    private http: HttpClient,
    private router: Router,
    private logoutService: LogoutService,
    private toastr: ToastrService
  ) { }

  dataset = [];
  user = JSON.parse(localStorage.getItem('user'));

  ngOnInit() {
    if(this.user){
      return this.http.get(URLS.mainfeed, {headers: new HttpHeaders({'Authorization': 'Token '+ this.user.auth_token})})
        .subscribe(res => {
          console.log(res['results']);
          this.dataset= res['results'];
        })
      }else{
        this.router.navigate(['/', 'signup']);
    }

  }

  logout(){
    this.logoutService.logout()
      .subscribe(
        next => {
          if (next === true){
            localStorage.clear();
            this.router.navigate(['/', 'login']);
          }else{
            this.toastr.error("Logout failed!!!");
            console.log("failed");
          }
        }
      )
   

  }

}
