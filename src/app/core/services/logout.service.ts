import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map, catchError } from "rxjs/operators";
import { URLS } from "../services/constants.service";
import { of } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class LogoutService {

  constructor(private http:HttpClient) { }

  payload = [];
  user = JSON.parse(localStorage.getItem('user'));

  logout(){
    return this.http.post(URLS.logout, this.payload, {headers: new HttpHeaders({'Authorization': 'Token '+ this.user.auth_token})})
      .pipe(
        map(res => {
          if(res['results'] != null){
            console.log(res['results']);
            return true;
          }else{
            return false;
          }
        }),
        catchError((err: any) => {
          console.log("Logout failed: "+ err.error);
          return of (false);
        })
      )
  }
}