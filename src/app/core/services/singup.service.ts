import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, catchError } from "rxjs/operators";
import { URLS } from "../services/constants.service";
import { of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SingupService {

  constructor(
    private http: HttpClient,
  ) { }

  signup(payload){
    return this.http.post(URLS.singup, payload)
      .pipe(
        map(res => {
          if(res['auth_token'] != null){
            let user = (res);
            localStorage.setItem('user', JSON.stringify(user));
            console.log(res['auth_token']);
            return true;
          }else{
            return false;
          }
        }),
        catchError((err: any) => {
          console.log("signup failed: "+ err.error);
          return of (false);
        })
      )
  }
}
