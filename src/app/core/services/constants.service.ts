let BASE_URL = "https://pentiumtech-api.herokuapp.com/api/v1.0/";

const URLS = {
  singup: BASE_URL + 'users/signup/',
  mainfeed: BASE_URL + 'mainfeed/',
  lgoin: BASE_URL + 'users/login/',
  logout: BASE_URL + 'users/logout/'
};

export {
  URLS
};