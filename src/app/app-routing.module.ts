import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginComponent } from './modules/home/components/login/login.component';
import { SignupComponent } from './modules/home/components/signup/signup.component';
import { WelcomeComponent } from './modules/home/components/welcome/welcome.component';
import { Page404Component } from './modules/home/page404/page404.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'signup',
    pathMatch: 'full'
  },
  {
    path: '*',
    redirectTo: 'signup',
    pathMatch: 'full'
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'signup',
    component: SignupComponent
  },
  {
    path: 'gallery',
    component: WelcomeComponent
  },
  {
    path: '404ERROR',
    component: Page404Component
  },
  {
    path: '**',
    redirectTo: '404ERROR',
    pathMatch: "full"
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
